# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'flag/icon/css/rails/version'

Gem::Specification.new do |spec|
  spec.name          = "flag-icon-css-rails"
  spec.version       = Flag::Icon::Css::Rails::VERSION
  spec.authors       = ["Balazs Kovacs"]
  spec.email         = ["balazs.kovacs@jbslabs.com"]
  spec.summary       = %q{CSS for vector based country flags!}
  spec.description   = %q{CSS for vector based country flags! - Rails asset pipeline integration}
  spec.homepage      = "http://lipis.github.io/flag-icon-css/"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "railties", "~> 3.1"
  spec.add_development_dependency "rake"
end
